// Continue at:
// https://nextjs.org/learn/basics/navigate-between-pages/link-with-a-button

// This is the Link API
import Link from 'next/link'

const Index = () => (
  <div>
    <Link href="/about">
      <a style={{ fontSize: 20 }}>About Page</a>
    </Link>
    <p>Hello Next.js</p>
  </div>
)

export default Index
